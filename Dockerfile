FROM php:8.2-fpm

# Set the working directory in the container
WORKDIR /var/www

# Install dependencies
RUN apt-get update \
    && apt-get install -y \
        libzip-dev \
        unzip \
        libonig-dev \
        libxml2-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install pdo_mysql zip mbstring exif pcntl bcmath soap

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy the Laravel application into the container
COPY new-helloworld /var/www

# Install Laravel dependencies
RUN composer install --no-dev --optimize-autoloader

# Generate autoload files
RUN composer dump-autoload --no-scripts --optimize

# Set permissions
RUN chown -R www-data:www-data storage bootstrap/cache
