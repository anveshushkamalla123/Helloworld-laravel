variable "aws_region" {
  description = "AWS region"
  default     = "us-east-2"
}

variable "vpc1_cidr_block" {
  description = "CIDR block for VPC1"
  default     = "172.32.0.0/16"
}

variable "vpc2_cidr_block" {
  description = "CIDR block for VPC2"
  default     = "172.31.0.0/16"
}

variable "eks_cluster_subnet_cidr_blocks" {
  description = "CIDR blocks for EKS cluster subnets"
  default     = ["172.32.1.0/24", "172.32.2.0/24"]
}

variable "rds_subnet_cidr_blocks" {
  description = "CIDR blocks for RDS subnets"
  default     = ["172.31.1.0/24", "172.31.2.0/24"]
}

variable "key_name" {
  description = "EC2 key pair name"
  default     = "laravel-app"
}

variable "ami_id" {
  description = "AMI ID for EC2 instances"
  default     = "ami-xxxxxxxxxxxxxxxxx"
}
