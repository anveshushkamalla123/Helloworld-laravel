# Outputs
output "vpc1_id" {
  value = aws_vpc.vpc1.id
}

output "vpc2_id" {
  value = aws_vpc.vpc2.id
}

output "eks_cluster_autoscaling_group_name" {
  value = aws_autoscaling_group.eks_cluster_autoscaling_group.name
}

output "rds_instance_endpoint" {
  value = aws_db_instance.rds_instance.endpoint
}
