provider "aws" {
  region = var.aws_region
}

# VPC 1
resource "aws_vpc" "vpc1" {
  cidr_block = var.vpc1_cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "VPC1"
  }
}

# VPC 2
resource "aws_vpc" "vpc2" {
  cidr_block = var.vpc2_cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "VPC2"
  }
}

# Peering Connection
resource "aws_vpc_peering_connection" "vpc_peering" {
  vpc_id = aws_vpc.vpc1.id
  peer_vpc_id = aws_vpc.vpc2.id

  tags = {
    Name = "VPC-Peering-Connection"
  }
}

# Route between VPCs
resource "aws_route" "route_to_vpc2" {
  route_table_id            = aws_vpc.vpc1.main_route_table_id
  destination_cidr_block    = var.vpc2_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc_peering.id
}

# Subnets for EKS Cluster
resource "aws_subnet" "eks_cluster_subnet" {
  count = length(var.eks_cluster_subnet_cidr_blocks)

  vpc_id                  = aws_vpc.vpc1.id
  cidr_block              = var.eks_cluster_subnet_cidr_blocks[count.index]
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "eks-cluster-subnet-${count.index}"
  }
}

# Subnets for RDS
resource "aws_subnet" "rds_subnet" {
  count = length(var.rds_subnet_cidr_blocks)

  vpc_id                  = aws_vpc.vpc2.id
  cidr_block              = var.rds_subnet_cidr_blocks[count.index]
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "rds-subnet-${count.index}"
  }
}

# Security Group for RDS in VPC2
resource "aws_security_group" "rds_security_group" {
  name        = "rds-security-group"
  description = "Security group for RDS in VPC2"
  vpc_id      = aws_vpc.vpc2.id

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# RDS Instance in VPC2
resource "aws_db_instance" "rds_instance" {
  identifier           = "my-rds-instance"
  allocated_storage    = 20
  engine               = "mysql"
  master_username      = "admin"
  master_password      = "admin123"
  instance_class       = "db.t2.micro"
  vpc_security_group_ids = [aws_security_group.rds_security_group.id]
  subnet_group_name    = aws_db_subnet_group.rds_subnet_group.name
}

# Subnet Group for RDS
resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "rds-subnet-group"
  subnet_ids = aws_subnet.rds_subnet[*].id
}

# Security Group for EKS Cluster in VPC1
resource "aws_security_group" "eks_cluster_security_group" {
  name        = "eks-cluster-security-group"
  description = "Security group for EKS Cluster in VPC1"
  vpc_id      = aws_vpc.vpc1.id
}

# Auto Scaling Group for K8s Cluster in VPC1
resource "aws_autoscaling_group" "eks_cluster_autoscaling_group" {
  desired_capacity     = 2
  max_size             = 4
  min_size             = 2
  launch_configuration = aws_launch_configuration.eks_cluster_launch_config.id
  vpc_zone_identifier  = aws_subnet.eks_cluster_subnet[*].id
}

# Launch Configuration
resource "aws_launch_configuration" "eks_cluster_launch_config" {
  name                 = "eks-cluster-launch-config"
  image_id             = var.ami_id
  instance_type        = "t2.micro"
  key_name             = var.key_name
  security_groups      = [aws_security_group.eks_cluster_security_group.id]
  associate_public_ip_address = true
  lifecycle {
    create_before_destroy = true
  }
}

